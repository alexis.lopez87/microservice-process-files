import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { MicroserviceOptions } from '@nestjs/microservices';
import { GCPubSubServer } from 'nestjs-google-pubsub-microservice';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));

  const config = new DocumentBuilder()
    .setTitle('Affiliate example')
    .setDescription('The affiliates API description')
    .setVersion('1.0')
    .addTag('affiliates')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  const configService = new ConfigService();
  const suscriptionName = configService.get<string>('SUBSCRIPTION_NAME');
  const topicName = configService.get<string>('TOPIC_NAME');
  const projectId = configService.get<string>('GCP_PROJ_ID');

  app.connectMicroservice<MicroserviceOptions>({
    strategy: new GCPubSubServer({
      topic: topicName,
      subscription: suscriptionName,
      noAck: false,
      client: {
        projectId,
      },
    }),
  });

  await app.startAllMicroservices();

  await app.listen(3000);
}
bootstrap();

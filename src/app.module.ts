import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ApplicationModule } from './application/application.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ErrorInterceptors } from './interceptors/errors.interceptor';
import { APP_INTERCEPTOR } from '@nestjs/core';
import typeorm from './application/infraestructure/database/data-source';
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        configService.get('typeorm'),
    }),
    ConfigModule.forRoot({ isGlobal: true, load: [typeorm] }),
    ApplicationModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    { provide: APP_INTERCEPTOR, useClass: ErrorInterceptors },
  ],
})
export class AppModule {}

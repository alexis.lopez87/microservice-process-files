import { ApplicationDTO } from 'src/application/use-cases/dto/application.dto';
import { ApplicationDomain } from '../entities/application';
import { TaskDomain } from '../entities/task';

export interface iApplicationsUseCases {
  create(dtoApplication: ApplicationDTO): Promise<Partial<ApplicationDomain>>;
  createMany(filename: string): Promise<void>;
  uploadFile(originalNameFile: string, savedNameFile: string): Promise<void>;
  getApplications(): Promise<Partial<ApplicationDomain>[]>;
  getTasks(): Promise<Partial<TaskDomain>[]>;
}

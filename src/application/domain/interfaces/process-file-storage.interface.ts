export interface iProcessStorageFiles {
  downloadFile(
    bucketName: string,
    fileName: string,
    filePath: string,
  ): Promise<void>;
  uploadFile(
    bucketName: string,
    filePath: string,
    destFileName: string,
    username: string,
    generationMatchPrecondition: number,
  ): Promise<void>;
}

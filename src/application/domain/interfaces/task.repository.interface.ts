import { TaskDomain } from '../entities/task';

export interface iTaskRepository {
  getTasks(): Promise<Partial<TaskDomain>[]>;
}

import { ApplicationDomain } from '../entities/application';

export interface iApplicationRepository {
  create(
    application: Partial<ApplicationDomain>,
  ): Promise<Partial<ApplicationDomain>>;
  createMany(
    applications: Partial<ApplicationDomain>[],
    applicationsWithError: Record<number, string>,
    taskId: number,
    fileNameUploaded: string,
  ): Promise<void>;
  getApplications(): Promise<Partial<ApplicationDomain>[]>;
}

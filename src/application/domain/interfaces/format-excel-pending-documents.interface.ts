export interface FormatExcelPendingDocuments {
  Periodo: string;
  IDEOPE: number;
  NUMIMPRE: number;
  NUMIMPRELA: number;
  AFIRUT: number;
  AFIRUTDV: string;
  LICOBS1: number;
  Rut_Empresa: string | undefined;
  Fecha_inicio_LM: number | undefined;
  TIPO_SUBSIDIO: number | undefined;
  TIPO_CONVENIO: number | undefined;
  PERIODO_1: string | undefined;
  PERIODO_2: string | undefined;
  PERIODO_3: string | undefined;
  PERIODO_4: string | undefined;
  PERIODO_5: string | undefined;
  PERIODO_6: string | undefined;
  TIPO_DOCUMENTO_ADICIONAL_1: number | undefined;
  TIPO_DOCUMENTO_ADICIONAL_2: number | undefined;
  LICAMPLET: string | undefined;
  AFISUBTIP: number | undefined;
  LICCONIND: number | undefined;
}

import { ApplicationDomain } from '../entities/application';

export interface iProcessFiles {
  processExcel(filename: string): Promise<{
    newApplications: Partial<ApplicationDomain>[];
    applicationsWithError: Record<number, string>;
    taskId: number;
    fileNameUploaded: string;
  }>;
  uploadFile(originalNameFile: string, savedNameFile: string): Promise<void>;
}

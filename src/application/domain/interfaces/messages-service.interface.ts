export interface iMessagesService {
  sendMessage<T>(topicName: string, kind: string, payload: T): Promise<void>;
  subscribeToTopic(
    topicName: string,
    subscriptionName: string,
    handleMessage: (message: string) => void,
  ): Promise<void>;
}

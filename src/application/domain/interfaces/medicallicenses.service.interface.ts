import { MedicalLicenseDTO } from 'src/application/use-cases/dto/medicallicenses.dto';

export interface IMedicallicensesService {
  getLicensesByAffiliates(
    affiliateRuts: number[],
  ): Promise<MedicalLicenseDTO[]>;
}

export enum eOldDocumentType {
  salarySettlement1 = 5,
  salarySettlement2 = 96,
  nro42 = 42,
  certificateCreatedByEmployer = 97,
  updatedIncomeCertificate = 59,
}

export enum eStatusTasks {
  initiated = 'iniciado',
  finalized = 'finalized',
}

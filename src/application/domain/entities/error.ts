import { Injectable } from '@nestjs/common';

@Injectable()
export class ErrorDomain {
  id: number;
  message: string;
}

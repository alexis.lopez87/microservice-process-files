import { TypeDocumentDomain } from './type-document';

export class DocumentDomain {
  description: string;
  period?: string;
  employerRut?: string;
  typeDocument: TypeDocumentDomain;
}

import { Inject, Injectable } from '@nestjs/common';
import { ErrorDomain } from './error';
import { iTaskRepository } from '../interfaces/task.repository.interface';
import { keyTaskRepository } from 'src/application/libs/constants';

@Injectable()
export class TaskDomain {
  id: number;
  status: string;
  creationDate: Date;
  endDate: Date;
  errors: ErrorDomain[];
  processId: string;

  constructor(
    @Inject(keyTaskRepository)
    private readonly taskRepo: iTaskRepository,
  ) {}

  async getTasks() {
    return await this.taskRepo.getTasks();
  }
}

import { Injectable } from '@nestjs/common';

@Injectable()
export class DocumentUploadHistoryDomain {
  id: number;
  username: string;
  namefile: string;
  uploadDate: Date;
}

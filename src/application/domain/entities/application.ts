import { Inject, Injectable } from '@nestjs/common';
import { DocumentDomain } from './document';
import { ValidationError } from 'src/application/libs/errors';
import { DocumentUploadHistoryDomain } from './document-upload-history';
import {
  keyApplicationRepository,
  keyProcessFile,
} from 'src/application/libs/constants';
import { iApplicationRepository } from '../interfaces/application.repository.interface';
import { iProcessFiles } from '../interfaces/process-files.interface';
@Injectable()
export class ApplicationDomain {
  affiliateRut: number;
  affiliateRutDV: string;
  medicalLicenseNumber: number;
  creationDate: Date;
  endingDate?: Date;
  documents: DocumentDomain[];
  isContinue: boolean;
  hasNotification: boolean;
  userName: string;
  status: string;
  history: DocumentUploadHistoryDomain;

  constructor(
    @Inject(keyApplicationRepository)
    private readonly applicationRepo: iApplicationRepository,
    @Inject(keyProcessFile)
    private readonly processFile: iProcessFiles,
  ) {}

  async create(application: Partial<ApplicationDomain>) {
    if (application.affiliateRut < 0) {
      throw new ValidationError(
        'Solicitud debe tener un rut de afiliado válido',
      );
    }

    if (!application.documents || application.documents?.length === 0) {
      throw new ValidationError('Solicitud debe tener al menos un documento');
    }

    const newAplication = await this.applicationRepo.create(application);

    return newAplication;
  }

  async createMany(filename: string) {
    const { newApplications, applicationsWithError, taskId, fileNameUploaded } =
      await this.processFile.processExcel(filename);
    await this.applicationRepo.createMany(
      newApplications,
      applicationsWithError,
      taskId,
      fileNameUploaded,
    );
  }

  async uploadFile(originalNameFile: string, savedNameFile: string) {
    return await this.processFile.uploadFile(originalNameFile, savedNameFile);
  }

  async getApplications() {
    return await this.applicationRepo.getApplications();
  }
}

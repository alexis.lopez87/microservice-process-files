import { Module } from '@nestjs/common';
import { ApplicationController } from './application.controller';
import {
  keyApplicationRepository,
  keyApplicationsUseCases,
  keyMedicalLicensesService,
  keyMessagesService,
  keyProcessFile,
  keyStorageFile,
  keyTaskRepository,
} from './libs/constants';
import { PubSubMessages } from './infraestructure/messages/pubsub.messages';
import { ApplicationRepository } from './infraestructure/database/repositories/application.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentEntity } from './infraestructure/database/entities/document.entity';
import { ApplicationsUseCases } from './use-cases/applications.use-case';
import { ApplicationDomain } from './domain/entities/application';
import { ApplicationEntity } from './infraestructure/database/entities/application.entity';
import { ProcessFiles } from './infraestructure/files/process.files';
import { TypeDocumentEntity } from './infraestructure/database/entities/type-document.entity';
import { TypeDocumentHomologationEntity } from './infraestructure/database/entities/type-document-homologation.entity';
import { TasksEntity } from './infraestructure/database/entities/task.entity';
import { ErrorsEntity } from './infraestructure/database/entities/errors.entity';
import { StorageFiles } from './infraestructure/files/storage.files';
import { MedicalLicensesService } from './infraestructure/services/medical-licenses.service';
import { MulterModule } from '@nestjs/platform-express';
import { TaskDomain } from './domain/entities/task';
import { TaskRepository } from './infraestructure/database/repositories/task.repository';
import { HttpModule } from '@nestjs/axios';
import { DocumentUploadHistoryEntity } from './infraestructure/database/entities/document-upload-history.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ApplicationEntity,
      DocumentEntity,
      TypeDocumentEntity,
      TypeDocumentHomologationEntity,
      TasksEntity,
      ErrorsEntity,
      DocumentUploadHistoryEntity,
    ]),
    MulterModule.register({
      dest: './files',
    }),
    HttpModule,
  ],
  controllers: [ApplicationController],
  providers: [
    {
      provide: keyMedicalLicensesService,
      useClass: MedicalLicensesService,
    },
    {
      provide: keyStorageFile,
      useClass: StorageFiles,
    },
    {
      provide: keyProcessFile,
      useClass: ProcessFiles,
    },
    {
      provide: keyTaskRepository,
      useClass: TaskRepository,
    },
    {
      provide: keyApplicationRepository,
      useClass: ApplicationRepository,
    },
    TaskDomain,
    ApplicationDomain,
    {
      provide: keyApplicationsUseCases,
      useClass: ApplicationsUseCases,
    },
    {
      provide: keyMessagesService,
      useClass: PubSubMessages,
    },
  ],
})
export class ApplicationModule {}

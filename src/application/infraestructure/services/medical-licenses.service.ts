import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosError } from 'axios';
import { IMedicallicensesService } from 'src/application/domain/interfaces/medicallicenses.service.interface';
import { MedicalLicenseDTO } from 'src/application/use-cases/dto/medicallicenses.dto';

@Injectable()
export class MedicalLicensesService implements IMedicallicensesService {
  constructor(
    private configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  private URL_BASE = this.configService.get<string>('URL_BASE');

  public async getLicensesByAffiliates(affiliateRuts: number[]) {
    try {
      const queryString = affiliateRuts.map((key) => 'rut=' + key).join('&');
      const path = `${this.URL_BASE}/api/MedicalLicenses/affiliates?${queryString}`;

      const licenses = await this.httpService.axiosRef.get<MedicalLicenseDTO[]>(
        path,
      );

      return licenses.data;
    } catch (error) {
      const err = error as AxiosError;
      console.error('error al obtener licencias por afiliado', err.message);
      throw err;
    }
  }
}

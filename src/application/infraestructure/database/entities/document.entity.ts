import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApplicationEntity } from './application.entity';
import { TypeDocumentEntity } from './type-document.entity';

@Entity({ name: 'Documento' })
export class DocumentEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 500, name: 'descripcion', nullable: true })
  description: string;
  @Column({ name: 'periodo', nullable: true })
  period?: string;
  @Column({ name: 'rut-empleador', nullable: true })
  employerRut?: string;
  @ManyToOne(() => ApplicationEntity, (application) => application.documents)
  application: ApplicationEntity;
  @ManyToOne(() => TypeDocumentEntity, (typeDocument) => typeDocument.documents)
  typeDocument: TypeDocumentEntity;
}

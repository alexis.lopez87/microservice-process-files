/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DocumentEntity } from './document.entity';
import { DocumentUploadHistoryEntity } from './document-upload-history.entity';

@Entity('Solicitud')
export class ApplicationEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;
  @Column({ name: 'rut-afiliado' })
  affiliateRut: number;
  @Column({ name: 'rut-afiliado-dv', length: 1 })
  affiliateRutDV: string;
  @Column({ name: 'id-licencia' })
  medicalLicenseNumber: number;
  //@Column({ name: 'id-resolutor_evaluacion' })
  //solverId: number;
  @Column({ name: 'fch-creacion' })
  creationDate: Date;
  @Column({ name: 'fch-finalizacion', nullable: true })
  endingDate?: Date;
  @Column({ name: 'continua' })
  isContinue: boolean;
  @Column({ name: 'tiene-notificaciones' })
  hasNotification: boolean;
  @Column({ name: 'usuario-creador' })
  userName: string;
  @Column({ name: 'est' })
  status: string;
  @OneToMany((type) => DocumentEntity, (document) => document.application)
  documents: DocumentEntity[];
  @ManyToOne(
    () => DocumentUploadHistoryEntity,
    (history) => history.applications,
  )
  history: DocumentUploadHistoryEntity;
}

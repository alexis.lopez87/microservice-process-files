import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TasksEntity } from './task.entity';

@Entity({ name: 'Errores' })
export class ErrorsEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 500, name: 'mensaje' })
  message: string;
  @ManyToOne(() => TasksEntity, (task) => task.errors)
  task: TasksEntity;
}

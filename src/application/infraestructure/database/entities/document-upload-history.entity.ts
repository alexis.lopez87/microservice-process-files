import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ApplicationEntity } from './application.entity';

@Entity({ name: 'HistorialCargaDocumento' })
export class DocumentUploadHistoryEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ name: 'usuario' })
  username: string;
  @Column({ name: 'nombre-archivo' })
  namefile: string;
  @Column({ name: 'fch-carga' })
  uploadDate: Date;
  @OneToMany((type) => ApplicationEntity, (application) => application.history)
  applications: ApplicationEntity[];
}

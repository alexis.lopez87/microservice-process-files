/* eslint-disable @typescript-eslint/no-unused-vars */
import { Column, Entity, OneToMany } from 'typeorm';
import { DocumentEntity } from './document.entity';
import { TypeDocumentHomologationEntity } from './type-document-homologation.entity';

@Entity({ name: 'TipoDocumento' })
export class TypeDocumentEntity {
  @Column({
    name: 'cdg',
    primary: true,
    unique: true,
  })
  code: number;
  @Column({ length: 100, name: 'nombre_tipo_documento' })
  name: string;
  @OneToMany((type) => DocumentEntity, (document) => document.typeDocument)
  documents: DocumentEntity[];
  @OneToMany(
    (typeDocumentHomologation) => TypeDocumentHomologationEntity,
    (typeDocumentHomologation) => typeDocumentHomologation.typeDocument,
  )
  homologations: TypeDocumentHomologationEntity[];
}

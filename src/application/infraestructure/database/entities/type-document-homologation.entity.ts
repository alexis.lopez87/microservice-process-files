import { Column, Entity, ManyToOne } from 'typeorm';
import { TypeDocumentEntity } from './type-document.entity';

@Entity({ name: 'HomologacionTipoDocumento' })
export class TypeDocumentHomologationEntity {
  @Column({ name: 'cdg', unique: true, primary: true })
  code: number;

  @Column({ name: 'glosa' })
  name: string;

  @ManyToOne(
    () => TypeDocumentEntity,
    (typeDocument) => typeDocument.homologations,
  )
  typeDocument: TypeDocumentEntity;
}

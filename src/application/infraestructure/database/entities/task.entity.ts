import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ErrorsEntity } from './errors.entity';

@Entity({ name: 'Tareas' })
export class TasksEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 50, name: 'estado' })
  status: string;
  @Column({ name: 'fch-creacion' })
  creationDate: Date;
  @Column({ name: 'fch-finalizacion', nullable: true })
  endDate: Date;
  @Column({ length: 50, name: 'id-proceso' })
  processId: string;
  @OneToMany((type) => ErrorsEntity, (errors) => errors.task)
  errors: ErrorsEntity[];
}

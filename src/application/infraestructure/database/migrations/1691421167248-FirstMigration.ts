import { MigrationInterface, QueryRunner } from 'typeorm';

export class FirstMigration1691421167248 implements MigrationInterface {
  name = 'FirstMigration1691421167248';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "HomologacionTipoDocumento" ("cdg" integer NOT NULL, "glosa" character varying NOT NULL, "typeDocumentCode" integer, CONSTRAINT "UQ_090724d8d82418cac7816f9c861" UNIQUE ("cdg"), CONSTRAINT "PK_090724d8d82418cac7816f9c861" PRIMARY KEY ("cdg"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "TipoDocumento" ("cdg" integer NOT NULL, "nombre_tipo_documento" character varying(100) NOT NULL, CONSTRAINT "UQ_ddd90837563f3fe2de4240ae2a0" UNIQUE ("cdg"), CONSTRAINT "PK_ddd90837563f3fe2de4240ae2a0" PRIMARY KEY ("cdg"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "Documento" ("id" SERIAL NOT NULL, "descripcion" character varying(500), "periodo" character varying, "rut-empleador" character varying, "applicationId" integer, "typeDocumentCode" integer, CONSTRAINT "PK_6b5e09a79fa50bb421a44fc459e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "Solicitud" ("id" SERIAL NOT NULL, "rut-afiliado" integer NOT NULL, "rut-afiliado-dv" character varying(1) NOT NULL, "id-licencia" integer NOT NULL, "fch-creacion" TIMESTAMP NOT NULL, "fch-finalizacion" TIMESTAMP, "continua" boolean NOT NULL, "tiene-notificaciones" boolean NOT NULL, "usuario-creador" character varying NOT NULL, "est" character varying NOT NULL, "historyId" integer, CONSTRAINT "PK_1b56183d50579ed8e29e0801c3a" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "HistorialCargaDocumento" ("id" SERIAL NOT NULL, "usuario" character varying NOT NULL, "nombre-archivo" character varying NOT NULL, "fch-carga" TIMESTAMP NOT NULL, CONSTRAINT "PK_f03c4f7585fa6e2518762a90036" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "Tareas" ("id" SERIAL NOT NULL, "estado" character varying(50) NOT NULL, "fch-creacion" TIMESTAMP NOT NULL, "fch-finalizacion" TIMESTAMP, "id-proceso" character varying(50) NOT NULL, CONSTRAINT "PK_346345d72cde708c94200c308a7" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "Errores" ("id" SERIAL NOT NULL, "mensaje" character varying(500) NOT NULL, "taskId" integer, CONSTRAINT "PK_29511f2c71a97771896591f452c" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "HomologacionTipoDocumento" ADD CONSTRAINT "FK_e42fa49f41d9c60aad79d360a58" FOREIGN KEY ("typeDocumentCode") REFERENCES "TipoDocumento"("cdg") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "Documento" ADD CONSTRAINT "FK_2cbff0f6778b2c36173618086b8" FOREIGN KEY ("applicationId") REFERENCES "Solicitud"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "Documento" ADD CONSTRAINT "FK_5f0985c673350d01a1dad9d1bd4" FOREIGN KEY ("typeDocumentCode") REFERENCES "TipoDocumento"("cdg") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "Solicitud" ADD CONSTRAINT "FK_e1c42aec83d4dce2c74191fcc46" FOREIGN KEY ("historyId") REFERENCES "HistorialCargaDocumento"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "Errores" ADD CONSTRAINT "FK_2faf94766ba87ee48b8cc7c0455" FOREIGN KEY ("taskId") REFERENCES "Tareas"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "Errores" DROP CONSTRAINT "FK_2faf94766ba87ee48b8cc7c0455"`,
    );
    await queryRunner.query(
      `ALTER TABLE "Solicitud" DROP CONSTRAINT "FK_e1c42aec83d4dce2c74191fcc46"`,
    );
    await queryRunner.query(
      `ALTER TABLE "Documento" DROP CONSTRAINT "FK_5f0985c673350d01a1dad9d1bd4"`,
    );
    await queryRunner.query(
      `ALTER TABLE "Documento" DROP CONSTRAINT "FK_2cbff0f6778b2c36173618086b8"`,
    );
    await queryRunner.query(
      `ALTER TABLE "HomologacionTipoDocumento" DROP CONSTRAINT "FK_e42fa49f41d9c60aad79d360a58"`,
    );
    await queryRunner.query(`DROP TABLE "Errores"`);
    await queryRunner.query(`DROP TABLE "Tareas"`);
    await queryRunner.query(`DROP TABLE "HistorialCargaDocumento"`);
    await queryRunner.query(`DROP TABLE "Solicitud"`);
    await queryRunner.query(`DROP TABLE "Documento"`);
    await queryRunner.query(`DROP TABLE "TipoDocumento"`);
    await queryRunner.query(`DROP TABLE "HomologacionTipoDocumento"`);
  }
}

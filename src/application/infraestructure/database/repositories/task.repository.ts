import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskDomain } from 'src/application/domain/entities/task';
import { iTaskRepository } from 'src/application/domain/interfaces/task.repository.interface';
import { TasksEntity } from '../entities/task.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TaskRepository implements iTaskRepository {
  constructor(
    @InjectRepository(TasksEntity)
    private taskRepository: Repository<TasksEntity>,
  ) {}

  async getTasks(): Promise<Partial<TaskDomain>[]> {
    return await this.taskRepository.find({
      relations: {
        errors: true,
      },
    });
  }
}

import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { iApplicationRepository } from 'src/application/domain/interfaces/application.repository.interface';
import { DataSource, Repository } from 'typeorm';
import { ApplicationEntity } from '../entities/application.entity';
import { ApplicationDomain } from 'src/application/domain/entities/application';
import { ValidationError } from 'src/application/libs/errors';
import { DocumentEntity } from '../entities/document.entity';
import { TypeDocumentEntity } from '../entities/type-document.entity';
import { TypeDocumentHomologationEntity } from 'src/application/infraestructure/database/entities/type-document-homologation.entity';
import { TasksEntity } from '../entities/task.entity';
import { ErrorsEntity } from '../entities/errors.entity';
import { eStatusTasks } from 'src/application/domain/enums/enum-old-type-document';
import { eApplicationStatus } from 'src/application/domain/enums/enum-document';
import { DocumentUploadHistoryEntity } from '../entities/document-upload-history.entity';
import { IMedicallicensesService } from 'src/application/domain/interfaces/medicallicenses.service.interface';
import { keyMedicalLicensesService } from 'src/application/libs/constants';

@Injectable()
export class ApplicationRepository implements iApplicationRepository {
  constructor(
    @InjectRepository(ApplicationEntity)
    private applicationsRepository: Repository<ApplicationEntity>,
    @InjectRepository(DocumentEntity)
    private documentsRepository: Repository<DocumentEntity>,
    @InjectRepository(TypeDocumentEntity)
    private typeDocumentsRepository: Repository<TypeDocumentEntity>,
    @InjectRepository(TypeDocumentHomologationEntity)
    private typeDocumentHomologationRepository: Repository<TypeDocumentHomologationEntity>,
    @InjectRepository(TasksEntity)
    private taskRepository: Repository<TasksEntity>,
    @InjectRepository(ErrorsEntity)
    private errorRepository: Repository<ErrorsEntity>,
    @InjectRepository(DocumentUploadHistoryEntity)
    private documentUploadHistoryRepository: Repository<DocumentUploadHistoryEntity>,
    private dataSource: DataSource,
    @Inject(keyMedicalLicensesService)
    private readonly medicalLicensesService: IMedicallicensesService,
  ) {}

  async create(application: Partial<ApplicationDomain>) {
    const findApplication = await this.applicationsRepository.findOneBy({
      affiliateRut: application.affiliateRut,
      medicalLicenseNumber: application.medicalLicenseNumber,
    });

    if (findApplication) {
      throw new ValidationError('Solicitud ya existe');
    }

    const applicationOrm = {
      affiliateRut: application.affiliateRut,
      medicalLicenseNumber: application.medicalLicenseNumber,
      creationDate: application.creationDate,
    };

    const newApplicationOrm = await this.applicationsRepository.save(
      applicationOrm,
    );

    newApplicationOrm.documents = [];

    return newApplicationOrm;
  }

  private async validateApplicationsIfHaveChangeTheirStatus(
    applications: Partial<ApplicationDomain>[],
  ): Promise<{
    finalApplications: Partial<ApplicationDomain>[];
    applicationsNotHasChangedTheirStatus: Record<number, string>;
  }> {
    const finalApplications: Partial<ApplicationDomain>[] = [];
    const applicationsNotHasChangedTheirStatus: Record<number, string> = {};

    const distinctRuts = new Set<number>();
    for (const application of applications) {
      distinctRuts.add(application.affiliateRut);
    }

    const licensesByAffiliates =
      await this.medicalLicensesService.getLicensesByAffiliates([
        ...distinctRuts,
      ]);

    if (licensesByAffiliates.length > 0) {
      for (const application of applications) {
        const medicalLicensesHasChangeItsState = licensesByAffiliates.find(
          (l) =>
            l.rut === application.affiliateRut &&
            l.rutDV === application.affiliateRutDV &&
            l.medicalLicenseNumber === application.medicalLicenseNumber,
        );
        if (medicalLicensesHasChangeItsState) {
          finalApplications.push(application);
        } else {
          applicationsNotHasChangedTheirStatus[
            application.medicalLicenseNumber
          ] = `Licencia ${application.medicalLicenseNumber} no ha actualizado su estado a pendiente de documentación`;
        }
      }
    } else {
      for (const application of applications) {
        applicationsNotHasChangedTheirStatus[
          application.medicalLicenseNumber
        ] = `Licencia ${application.medicalLicenseNumber} no ha actualizado su estado a pendiente de documentación`;
      }
    }

    return { finalApplications, applicationsNotHasChangedTheirStatus };
  }

  async createMany(
    applications: Partial<ApplicationDomain>[],
    applicationsWithError: Record<number, string>,
    taskId: number,
    fileNameUploaded: string,
  ): Promise<void> {
    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const typeDocuments = await this.typeDocumentsRepository.find();
      const typeDocumentHomologation =
        await this.typeDocumentHomologationRepository.find({
          relations: {
            typeDocument: true,
          },
        });

      if (typeDocuments.length === 0) {
        throw new ValidationError('Falta cargar los tipos de documento');
      }

      if (typeDocumentHomologation.length === 0) {
        throw new ValidationError(
          'Falta cargar los tipos de documento de homologación',
        );
      }

      //TODO: descomentar cuando se pueda conectar a as400
      //const { finalApplications, applicationsNotHasChangedTheirStatus } =
      //await this.validateApplicationsIfHaveChangeTheirStatus(applications);

      const newUploadHistory = await this.documentUploadHistoryRepository.save({
        namefile: fileNameUploaded,
        uploadDate: new Date(),
        username: 'usuario1', //TODO: usuario en duro
      });

      //TODO: cambiar applications a finalApplications cuando se pueda conectar a as400
      for (const application of applications) {
        const applicationEntity: Partial<ApplicationEntity> = {
          affiliateRut: application.affiliateRut,
          affiliateRutDV: application.affiliateRutDV,
          medicalLicenseNumber: application.medicalLicenseNumber,
          creationDate: application.creationDate,
          isContinue: application.isContinue,
          hasNotification: application.hasNotification,
          userName: application.userName,
          status: eApplicationStatus.iniciated,
          history: newUploadHistory,
        };

        const existsApplication = await this.applicationsRepository.findOne({
          where: {
            affiliateRut: application.affiliateRut,
            affiliateRutDV: application.affiliateRutDV,
            medicalLicenseNumber: application.medicalLicenseNumber,
          },
        });

        if (existsApplication) {
          continue;
        }

        const newApplicationEntity = await this.applicationsRepository.save(
          applicationEntity,
        );

        for (const document of application.documents) {
          const typeDocumentForDoc = typeDocumentHomologation.find(
            (tdh) => tdh.code === document.typeDocument.code,
          );

          const typeDocument = typeDocuments.find(
            (td) => td.code === typeDocumentForDoc.typeDocument.code,
          );

          const newDocument: Partial<DocumentEntity> = {
            application: newApplicationEntity,
            description: document.description,
            period: document.period,
            typeDocument,
          };
          await this.documentsRepository.save(newDocument);
        }
      }

      const processTask = await this.taskRepository.findOneBy({ id: taskId });

      //TODO: descomentar cuando se pueda conectar a as400
      const finalApplicationsWithErrors: Record<number, string> = {
        ...applicationsWithError,
        //...applicationsNotHasChangedTheirStatus,
      };

      for (const medicalLicenseNumber in finalApplicationsWithErrors) {
        const message = applicationsWithError[medicalLicenseNumber];
        await this.errorRepository.save({ message, task: processTask });
      }

      await this.taskRepository.update(taskId, {
        status: eStatusTasks.finalized,
        endDate: new Date(),
      });
      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      const err = error as Error;
      await this.taskRepository.update(taskId, {
        status: eStatusTasks.finalized,
        endDate: new Date(),
      });
      const processTask = await this.taskRepository.findOneBy({ id: taskId });
      await this.errorRepository.save({
        message: err.message,
        task: processTask,
      });
      throw error;
    } finally {
      await queryRunner.release();
    }
  }

  async getApplications(): Promise<Partial<ApplicationDomain>[]> {
    return await this.applicationsRepository.find({
      relations: {
        documents: true,
      },
    });
  }
}

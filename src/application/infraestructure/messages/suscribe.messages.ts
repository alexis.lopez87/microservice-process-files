import { PubSub, Message as PubSubMessage } from '@google-cloud/pubsub';
//import { cloudConfig } from 'src/application/libs/config';
//import { ErrorReporting } from '@google-cloud/error-reporting';

export default function listenForMessages(subscriptionId: string) {
  /*
  const jsonPath = './../../authentic-block-318519-f3c04deb3932.json';
  const errorReporting = new ErrorReporting({
    projectId: cloudConfig.GCP_PROJECT_ID,
    keyFilename: jsonPath,
    credentials: require(jsonPath),
    logLevel: 'error',
    reportMode: 'always',
    serviceContext: {
      service: 'notifications',
      version: 'notifications-v1',
    },
  });
  */

  const pubSubClient = new PubSub();
  const subscription = pubSubClient.subscription(subscriptionId);

  const messageHandler = (message: PubSubMessage) => {
    try {
      const messageReceived = message.data.toString();
      console.log('messageReceived', messageReceived);
      console.log(`Attributes: ${JSON.stringify(message.attributes)}`);

      message.ack();
    } catch (error) {
      message.nack();
      console.error(`error en subscription`, error);

      /*
      const customError = `Error handling message with ID ${message.id}`;
      errorReporting.report(
        customError,
        {
          method: 'POST',
          statusCode: 500,
          url: 'sendNotificationHandler',
          referrer: `${customError} !!`,
        },
        error.stack,
        () => console.log('done!'),
      );
      */
    }
  };

  subscription.on('message', messageHandler);
}

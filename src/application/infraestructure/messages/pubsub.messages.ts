import { PubSub, Message } from '@google-cloud/pubsub';
import { Injectable } from '@nestjs/common';
//import { ConfigService } from '@nestjs/config';
import { iMessagesService } from 'src/application/domain/interfaces/messages-service.interface';

@Injectable()
export class PubSubMessages implements iMessagesService {
  private pubSubClient = new PubSub();

  async sendMessage<T>(
    topicName: string,
    kind: string,
    payload: T,
  ): Promise<void> {
    const dataBuffer = Buffer.from(JSON.stringify(payload));
    const messageId = await this.pubSubClient
      .topic(topicName)
      .publishMessage({ data: dataBuffer, attributes: { kind } });
    console.log(`Message ${messageId} published.`);
  }

  async subscribeToTopic(
    topicName: string,
    subscriptionName: string,
  ): Promise<void> {
    const topic = this.pubSubClient.topic(topicName);
    const subscription = topic.subscription(subscriptionName);

    subscription.on('message', async (message: Message) => {
      try {
        const messageReceived = message.data.toString();
        console.log('messageReceived', messageReceived);
        console.log(`Attributes: ${JSON.stringify(message.attributes)}`);

        //const filename = '';
        //await this.loadFileApplicationsUseCases.createMany(filename);
        message.ack();
      } catch (error) {
        message.nack();
        console.error('Error handling Pub/Sub message:', error);
      }
    });

    subscription.on('error', (error) => {
      console.error('Error occurred on Pub/Sub subscription:', error);
    });
  }
}

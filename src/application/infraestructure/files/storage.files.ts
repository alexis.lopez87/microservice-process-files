import { Injectable } from '@nestjs/common';
import { Storage } from '@google-cloud/storage';
import { iProcessStorageFiles } from 'src/application/domain/interfaces/process-file-storage.interface';

@Injectable()
export class StorageFiles implements iProcessStorageFiles {
  private storage = new Storage();

  async downloadFile(
    bucketName: string,
    fileName: string,
    filePath: string,
  ): Promise<void> {
    const options = {
      destination: filePath,
    };

    const file = this.storage.bucket(bucketName).file(fileName);
    const metadata = await file.getMetadata();
    console.log('metadata', metadata);

    await file.download(options);
  }

  async uploadFile(
    bucketName: string,
    filePath: string,
    destFileName: string,
    username: string,
    generationMatchPrecondition = 0,
  ): Promise<void> {
    const options = {
      destination: destFileName,
      metadata: {
        username,
      },
      // Optional:
      // Set a generation-match precondition to avoid potential race conditions
      // and data corruptions. The request to upload is aborted if the object's
      // generation number does not match your precondition. For a destination
      // object that does not yet exist, set the ifGenerationMatch precondition to 0
      // If the destination object already exists in your bucket, set instead a
      // generation-match precondition using its generation number.
      preconditionOpts: { ifGenerationMatch: generationMatchPrecondition },
    };
    const bucket = this.storage.bucket(bucketName);
    await bucket.upload(filePath, options);
  }
}

import { Inject, Injectable } from '@nestjs/common';
import { FormatExcelPendingDocuments } from 'src/application/domain/interfaces/format-excel-pending-documents.interface';
import { iProcessFiles } from 'src/application/domain/interfaces/process-files.interface';
import { ValidationError } from 'src/application/libs/errors';
import { readFile, utils } from 'xlsx';
import { join, extname, parse } from 'path';
import { ApplicationDomain } from 'src/application/domain/entities/application';
import {
  eOldDocumentType,
  eStatusTasks,
} from '../../domain/enums/enum-old-type-document';
import { TasksEntity } from '../database/entities/task.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { format } from 'date-fns';
import { resolve } from 'app-root-path';
import { iProcessStorageFiles } from 'src/application/domain/interfaces/process-file-storage.interface';
import { keyStorageFile } from 'src/application/libs/constants';

@Injectable()
export class ProcessFiles implements iProcessFiles {
  constructor(
    private readonly configService: ConfigService,
    @Inject(keyStorageFile)
    private readonly storageFiles: iProcessStorageFiles,
    @InjectRepository(TasksEntity)
    private taskRepository: Repository<TasksEntity>,
  ) {}

  private fileIsExcel(filename: string) {
    const extName = extname(filename);
    const validExtensions = ['.xls', '.xlsx'];
    return validExtensions.includes(extName);
  }

  private validateSubType(applicationExcel: FormatExcelPendingDocuments) {
    const applicationWithError: Record<number, string> = {};
    const maternity = [3, 4];
    const healing = [1, 7];
    const MIN_PERIODS_MATERNITY = 3;
    const MIN_PERIODS_HEALING = 6;

    if (maternity.includes(applicationExcel.AFISUBTIP)) {
      const props = Object.entries(applicationExcel);
      let cont = 0;
      for (const [key] of props) {
        if (key.includes('PERIODO_')) {
          cont++;
        }
      }

      if (cont < MIN_PERIODS_MATERNITY) {
        applicationWithError[
          applicationExcel.NUMIMPRELA
        ] = `Licencia médica ${applicationExcel.NUMIMPRELA} que es curativa, tiene menos de 3 periodos`;
      }
    } else if (healing.includes(applicationExcel.AFISUBTIP)) {
      const props = Object.entries(applicationExcel);
      let cont = 0;
      for (const [key] of props) {
        if (key.includes('PERIODO_')) {
          cont++;
        }
      }

      if (cont < MIN_PERIODS_HEALING) {
        applicationWithError[
          applicationExcel.NUMIMPRELA
        ] = `Licencia médica ${applicationExcel.NUMIMPRELA} que es maternal, tiene menos de 6 periodos`;
      }
    }

    return applicationWithError;
  }

  async processExcel(filename: string) {
    const customPromise = new Promise<{
      newApplications: Partial<ApplicationDomain>[];
      applicationsWithError: Record<number, string>;
      taskId: number;
      fileNameUploaded: string;
    }>(async (resolve, reject) => {
      const newTask = await this.taskRepository.save({
        status: eStatusTasks.initiated,
        creationDate: new Date(),
        processId: filename,
      });

      try {
        const bucketName = this.configService.get<string>('BUCKET_NAME');
        const tempPath = __dirname;
        const filePath = join(tempPath, filename);

        const isExcel = this.fileIsExcel(filename);
        if (!isExcel) {
          throw new ValidationError(`Archivo ${filename} debe ser excel`);
        }

        await this.storageFiles.downloadFile(bucketName, filename, filePath);

        const applicationsWithError: Record<number, string> = {};
        const newApplications: Partial<ApplicationDomain>[] = [];

        this.storageFiles.downloadFile(bucketName, filename, filePath);
        const file = readFile(filePath);

        if (file.SheetNames.length === 0) {
          throw new ValidationError(
            `Archivo ${filename} debe tener al menos una hoja`,
          );
        }

        const data = utils.sheet_to_json<FormatExcelPendingDocuments>(
          file.Sheets[file.SheetNames[0]],
        );

        if (data.length === 0) {
          throw new ValidationError(`Archivo ${filename} debe tener registros`);
        }

        for (const applicationExcel of data) {
          if (applicationExcel.LICOBS1 === eOldDocumentType.nro42) {
            continue;
          }

          const newApplicationTemplate: Partial<ApplicationDomain> = {
            affiliateRut: applicationExcel.AFIRUT,
            affiliateRutDV: applicationExcel.AFIRUTDV,
            medicalLicenseNumber: applicationExcel.NUMIMPRELA,
            creationDate: new Date(),
            documents: [],
            userName: 'usuario1', //TODO: usuario en duro
            status: 'Pendiente Afiliado',
          };

          const appWithErrorInSubType = this.validateSubType(applicationExcel);
          const propsErrorInSubType = Object.values(appWithErrorInSubType);
          if (propsErrorInSubType.length > 0) {
            for (const [
              medicalLicenseNumber,
              errorMessage,
            ] of propsErrorInSubType) {
              applicationsWithError[medicalLicenseNumber] = errorMessage;
            }
            continue;
          }

          if (!applicationExcel.LICAMPLET) {
            newApplicationTemplate.isContinue = false;
          } else {
            newApplicationTemplate.isContinue = true;
          }

          if (applicationExcel.LICCONIND === 0) {
            newApplicationTemplate.hasNotification = true;
          } else {
            newApplicationTemplate.hasNotification = false;
          }

          if (
            applicationExcel.LICOBS1 ===
            eOldDocumentType.certificateCreatedByEmployer
          ) {
            if (
              !applicationExcel.Rut_Empresa ||
              applicationExcel.Rut_Empresa === ''
            ) {
              applicationsWithError[
                applicationExcel.NUMIMPRELA
              ] = `Licencia médica ${applicationExcel.NUMIMPRELA} le falta rut de empleador`;
              continue;
            }

            newApplicationTemplate.documents.push({
              period: null,
              description: null,
              employerRut: applicationExcel.Rut_Empresa,
              typeDocument: {
                code: applicationExcel.LICOBS1,
                name: null,
              },
            });
          }

          if (
            (applicationExcel.LICOBS1 === eOldDocumentType.salarySettlement1 ||
              applicationExcel.LICOBS1 ===
                eOldDocumentType.salarySettlement2) &&
            !applicationExcel.PERIODO_1 &&
            !applicationExcel.PERIODO_2 &&
            !applicationExcel.PERIODO_3 &&
            !applicationExcel.PERIODO_4 &&
            !applicationExcel.PERIODO_5 &&
            !applicationExcel.PERIODO_6
          ) {
            applicationsWithError[
              applicationExcel.NUMIMPRELA
            ] = `Licencia médica ${applicationExcel.NUMIMPRELA} es liquidación de sueldo pero no tiene ningun periodo asociado`;
            continue;
          }

          if (
            applicationExcel.LICOBS1 === eOldDocumentType.salarySettlement1 ||
            applicationExcel.LICOBS1 === eOldDocumentType.salarySettlement2
          ) {
            if (applicationExcel.PERIODO_1) {
              newApplicationTemplate.documents.push({
                period: applicationExcel.PERIODO_1,
                description: null,
                employerRut: null,
                typeDocument: {
                  code: applicationExcel.LICOBS1,
                  name: null,
                },
              });
            }

            if (applicationExcel.PERIODO_2) {
              newApplicationTemplate.documents.push({
                period: applicationExcel.PERIODO_2,
                description: null,
                employerRut: null,
                typeDocument: {
                  code: applicationExcel.LICOBS1,
                  name: null,
                },
              });
            }

            if (applicationExcel.PERIODO_3) {
              newApplicationTemplate.documents.push({
                period: applicationExcel.PERIODO_3,
                description: null,
                employerRut: null,
                typeDocument: {
                  code: applicationExcel.LICOBS1,
                  name: null,
                },
              });
            }

            if (applicationExcel.PERIODO_4) {
              newApplicationTemplate.documents.push({
                period: applicationExcel.PERIODO_4,
                description: null,
                employerRut: null,
                typeDocument: {
                  code: applicationExcel.LICOBS1,
                  name: null,
                },
              });
            }

            if (applicationExcel.PERIODO_5) {
              newApplicationTemplate.documents.push({
                period: applicationExcel.PERIODO_5,
                description: null,
                employerRut: null,
                typeDocument: {
                  code: applicationExcel.LICOBS1,
                  name: null,
                },
              });
            }

            if (applicationExcel.PERIODO_6) {
              newApplicationTemplate.documents.push({
                period: applicationExcel.PERIODO_6,
                description: null,
                employerRut: null,
                typeDocument: {
                  code: applicationExcel.LICOBS1,
                  name: null,
                },
              });
            }
          }

          if (applicationExcel.TIPO_DOCUMENTO_ADICIONAL_1) {
            newApplicationTemplate.documents.push({
              period: null,
              description: null,
              employerRut: null,
              typeDocument: {
                code: applicationExcel.TIPO_DOCUMENTO_ADICIONAL_1,
                name: null,
              },
            });
          }

          if (applicationExcel.TIPO_DOCUMENTO_ADICIONAL_2) {
            newApplicationTemplate.documents.push({
              period: null,
              description: null,
              employerRut: null,
              typeDocument: {
                code: applicationExcel.TIPO_DOCUMENTO_ADICIONAL_2,
                name: null,
              },
            });
          }

          if (
            applicationExcel.LICOBS1 !== eOldDocumentType.salarySettlement1 &&
            applicationExcel.LICOBS1 !== eOldDocumentType.salarySettlement2 &&
            applicationExcel.LICOBS1 !==
              eOldDocumentType.certificateCreatedByEmployer
          ) {
            newApplicationTemplate.documents.push({
              period: null,
              description: null,
              employerRut: null,
              typeDocument: {
                code: applicationExcel.LICOBS1,
                name: null,
              },
            });
          }

          newApplications.push(newApplicationTemplate);
        }
        resolve({
          newApplications,
          applicationsWithError,
          taskId: newTask.id,
          fileNameUploaded: filename,
        });
      } catch (error) {
        await this.taskRepository.update(newTask.id, {
          status: eStatusTasks.finalized,
          endDate: new Date(),
        });
        reject(error);
      }
    });

    return await customPromise;
  }

  async uploadFile(originalNameFile: string, savedNameFile: string) {
    const bucketName = this.configService.get<string>('BUCKET_NAME');
    const tempPath = resolve('/files');
    const filePath = join(tempPath, savedNameFile);

    const fileName = parse(originalNameFile).name;
    const extName = extname(originalNameFile);

    const destFileName = `${fileName}-${format(
      new Date(),
      'yyyyMMddHHmmss',
    )}${extName}`;

    const username = 'usuario1'; //TODO: usuario en duro

    await this.storageFiles.uploadFile(
      bucketName,
      filePath,
      destFileName,
      username,
      0,
    );
  }
}

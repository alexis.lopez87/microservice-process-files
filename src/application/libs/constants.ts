export const keyMessagesService = 'keyMessagesService';
export const keyApplicationRepository = 'keyApplicationRepository';
export const keyApplicationsUseCases = 'keyApplicationsUseCases';
export const keyProcessFile = 'keyProcessFile';
export const keyMedicalLicensesService = 'keyMedicalLicensesService';
export const keyStorageFile = 'keyStorageFile';
export const keyTaskRepository = 'keyTaskRepository';

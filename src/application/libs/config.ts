export const cloudConfig = {
  GCP_PROJECT_ID: process.env.GCP_PROJECT_ID ?? '',
  SUBSCRIPTION_NAME: process.env.SUBSCRIPTION_NAME ?? '',
};

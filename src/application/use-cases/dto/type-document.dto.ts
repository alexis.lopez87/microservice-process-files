import { ApiProperty } from '@nestjs/swagger';

export class TypeDocumentDTO {
  @ApiProperty()
  code: number;
  @ApiProperty()
  name: string;
}

import { ApiProperty } from '@nestjs/swagger';

export class MedicalLicenseDTO {
  @ApiProperty()
  medicalLicenseNumber?: number;
  @ApiProperty()
  startDate: Date;
  @ApiProperty()
  endDate: Date;
  @ApiProperty()
  status: string;
  @ApiProperty()
  rut: number;
  @ApiProperty()
  rutDV: string;
}

import {
  IsNotEmpty,
  IsNumber,
  IsArray,
  IsOptional,
  IsDateString,
  IsString,
  IsBoolean,
} from 'class-validator';
import { DocumentDTO } from './document.dto';
import { ApiProperty } from '@nestjs/swagger';

export class ApplicationDTO {
  @ApiProperty()
  id?: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  affiliateRut: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  affiliateRutDV: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  medicalLicenseNumber: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  creationDate: Date;
  @ApiProperty()
  @IsDateString()
  @IsOptional()
  endingDate?: Date;
  @ApiProperty()
  @IsArray()
  @IsNotEmpty()
  documents: DocumentDTO[];
  @ApiProperty()
  @IsBoolean()
  @IsNotEmpty()
  isContinue: boolean;
  @ApiProperty()
  @IsBoolean()
  @IsNotEmpty()
  hasNotification: boolean;
}

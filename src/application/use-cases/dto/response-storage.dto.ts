export type ResponseStorageDTO = {
  kind: string;
  id: string;
  selfLink: string;
  name: string;
  bucket: string;
  generation: number;
  metageneration: number;
  contentType: string;
  timeCreated: string;
  updated: string;
  storageClass: string;
  timeStorageClassUpdated: string;
  size: number;
  md5Hash: string;
  mediaLink: string;
  crc32c: string;
  etag: string;
};

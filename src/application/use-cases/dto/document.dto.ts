import { ApiProperty } from '@nestjs/swagger';
import { TypeDocumentDTO } from './type-document.dto';

export class DocumentDTO {
  @ApiProperty()
  id?: number;
  @ApiProperty()
  description: string;
  @ApiProperty()
  period?: string;
  @ApiProperty()
  employerRut?: string;
  @ApiProperty()
  typeDocument: TypeDocumentDTO;
}

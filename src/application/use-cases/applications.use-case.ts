import { Injectable } from '@nestjs/common';
import { ApplicationDTO } from './dto/application.dto';
import { ApplicationDomain } from '../domain/entities/application';
import { TaskDomain } from '../domain/entities/task';
import { iApplicationsUseCases } from '../domain/interfaces/application.usecases.interface';

@Injectable()
export class ApplicationsUseCases implements iApplicationsUseCases {
  constructor(
    private readonly applicationDomain: ApplicationDomain,
    private readonly taskDomain: TaskDomain,
  ) {}

  public async create(dtoApplication: ApplicationDTO) {
    const newAplication = await this.applicationDomain.create(dtoApplication);
    return newAplication;
  }

  public async createMany(filename: string) {
    return await this.applicationDomain.createMany(filename);
  }

  public async uploadFile(originalNameFile: string, savedNameFile: string) {
    return await this.applicationDomain.uploadFile(
      originalNameFile,
      savedNameFile,
    );
  }

  public async getApplications() {
    return await this.applicationDomain.getApplications();
  }

  public async getTasks() {
    return await this.taskDomain.getTasks();
  }
}

import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Logger,
  MaxFileSizeValidator,
  ParseFilePipe,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ApplicationDTO } from './use-cases/dto/application.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';
import { Ctx, MessagePattern } from '@nestjs/microservices';
import { GCPubSubContext } from 'nestjs-google-pubsub-microservice';
import { ResponseStorageDTO } from './use-cases/dto/response-storage.dto';
import { iApplicationsUseCases } from './domain/interfaces/application.usecases.interface';
import { keyApplicationsUseCases } from './libs/constants';

@Controller('application')
export class ApplicationController {
  private readonly logger = new Logger(ApplicationController.name);
  constructor(
    @Inject(keyApplicationsUseCases)
    private readonly applicationsUseCases: iApplicationsUseCases,
  ) {}

  @Post('create')
  async create(@Body() dtoApplication: ApplicationDTO) {
    return await this.applicationsUseCases.create(dtoApplication);
  }

  @Get('read-excel')
  async createMany() {
    const filename = 'test.xlsx';
    return await this.applicationsUseCases.createMany(filename);
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          //new FileTypeValidator({ fileType: /(xls|xlsx)$/ }),
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 4 }),
        ],
      }),
    )
    file: Express.Multer.File,
  ) {
    const extName = extname(file.originalname);
    const validExtensions = ['.xls', '.xlsx'];
    if (!validExtensions.includes(extName)) {
      throw new HttpException(
        'Archivo debe ser excel',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    return await this.applicationsUseCases.uploadFile(
      file.originalname,
      file.filename,
    );
  }

  @Get('get-all')
  async getApplications() {
    return await this.applicationsUseCases.getApplications();
  }

  @Get('get-tasks')
  async getTasks() {
    return await this.applicationsUseCases.getTasks();
  }

  @MessagePattern(undefined)
  async getNotifications(@Ctx() context: GCPubSubContext) {
    const message = context.getMessage();
    const messageReceived = message.data.toString();
    const responseStorage = JSON.parse(messageReceived) as ResponseStorageDTO;

    try {
      await this.applicationsUseCases.createMany(responseStorage.name);
      message.ack();
    } catch (error) {
      const err = error as Error;
      this.logger.error(err.message);
      message.nack();
    }
  }
}
